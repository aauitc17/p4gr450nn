package neuralNetwork

import (
    "encoding/csv"
    "log"
    "os"
    "strconv"
    "io"
)
//standard filename add timestamp and evaluation
func (n *Network) SaveNetTo() string {
    if n.accuracy == "" {
        log.Fatal("network not evaluated")
    }
    path := "trainednetworks/"
    for _, val := range n.sizes {
        path = path + strconv.Itoa(val) + "_"
    }
    path = path + n.accuracy
    err := os.Mkdir(path, os.ModePerm)
    checkError("cannot make dir", err)
    for i, dat:= range n.weights {
        fpath := path + "/" + strconv.Itoa(i) + "weights" + ".csv"
        csvFile, err := os.Create(fpath)
        checkError("cannot create file", err)
        defer csvFile.Close()

        writer := csv.NewWriter(csvFile)
        defer writer.Flush()
        var datastr [][]string
        for _, row := range dat {
            var datastrstr []string
            for _, val := range row {
                s64 := strconv.FormatFloat(val, 'f', -1, 64)
                datastrstr = append(datastrstr, s64)
            }
            datastr = append(datastr, datastrstr)
        }

        for _, value := range datastr {
            err := writer.Write(value)
            checkError("Cannot write to file", err)
        }
    }
    for i, dat:= range n.biases {
        fpath := path + "/" + strconv.Itoa(i) + "biases" + ".csv"
        csvFile, err := os.Create(fpath)
        checkError("cannot create file", err)
        defer csvFile.Close()

        writer := csv.NewWriter(csvFile)
        defer writer.Flush()
        var datastr []string
        for _, val := range dat {
            s64 := strconv.FormatFloat(val, 'f', -1, 64)
            datastr = append(datastr, s64)
        }
        erro := writer.Write(datastr)
        checkError("Cannot write to file", erro)
    }
    return path
}

func checkError(message string, err error) {
    if err != nil {
        log.Fatal(message, err)
    }
}

func (n *Network) LoadNetFrom(folder string) {
    var size int
    for _, ch := range folder {
        if ch == 95 {
            size++
        }
    }
    for i := 0; i < size-1; i++ {
        wpath := folder + "/" + strconv.Itoa(i) + "weights" + ".csv"
        bpath := folder + "/" + strconv.Itoa(i) + "biases" + ".csv"
        wcsv, _ := os.Open(wpath)
        bcsv, _ := os.Open(bpath)
        wreader := csv.NewReader(wcsv)
        breader := csv.NewReader(bcsv)
        row := 0
        for {
            line, err := wreader.Read()
            if err == io.EOF {
                break
            }
            for j, w := range line {
                weight, err := strconv.ParseFloat(w, 64)
                checkError("cannot parse weight", err)
                n.weights[i][row][j] = weight
            }
            row++
        }
        for {
            line, err := breader.Read()
            if err == io.EOF {
                break
            }
            for j, b := range line {
                bias, err := strconv.ParseFloat(b, 64)
                checkError("cannot parse bias", err)
                n.biases[i][j] = bias
            }
        }
    }
}
