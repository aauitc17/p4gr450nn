package neuralNetwork

import (
	"math"
	"testing"

	"github.com/stretchr/testify/assert"
)

// func subtractmatrix(m1 [][]float64, m2 [][]float64) [][]float64 { ORDNET
// func scalematrix(val float64, m [][]float64) [][]float64 { ANDREAS
// func dot(w [][]float64, a []float64) []float64 { // ANDREAS

func copyODArray(b []float64) []float64 {
	a := append([]float64(nil), b...)
	return a
}

func copyTDArray(b [][]float64) [][]float64 {
	a := make([][]float64, len(b))
	for i := 0; i < len(b); i++ {
		a[i] = append([]float64(nil), b[i]...)
	}
	return a
}

func copyTDArrayInt(b [][]int64) [][]float64 {
	a := make([][]int64, len(b))
	for i := 0; i < len(b); i++ {
		a[i] = append([]int64(nil), b[i]...)
	}
	return a
}

// func add(a []float64, b []float64) []float64 {
func TestAdd(t *testing.T) {
	tt := []struct {
		name   string
		input1 []float64
		input2 []float64
		output []float64
	}{
		{
			name:   "basic add test",
			input1: []float64{1.0, 2.5},
			input2: []float64{2.3, 2.0},
			output: []float64{3.3, 4.5},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// backup our input arrays
			binput1 := copyODArray(tc.input1)
			binput2 := copyODArray(tc.input2)

			// validte main function functionality
			output := add(tc.input1, tc.input2)
			for i := 0; i < len(output); i++ {
				assert.Equal(t, tc.output[i], output[i], "Unexpected addition value")
			}

			// validate we did not modify our input array
			for i := 0; i < len(binput1); i++ {
				assert.Equal(t, tc.input1[i], binput1[i], "Unexpected - modified input array 1")
			}
			for i := 0; i < len(binput2); i++ {
				assert.Equal(t, tc.input2[i], binput2[i], "Unexpected - modified input array 2")
			}
		})

	}
}

// func transpose(m [][]float64) [][]float64 {
func TestTranspose(t *testing.T) {
	tt := []struct {
		name   string
		input  [][]float64
		output [][]float64
	}{
		{
			name: "Basic 2x2 transpose",
			input: [][]float64{
				{1.0, 2.0},
				{3.0, 4.0},
			},
			output: [][]float64{
				{1.0, 3.0},
				{2.0, 4.0},
			},
		}, {
			name: "Basic 2x3 transpose",
			input: [][]float64{
				{1.0, 2.0, 3.0},
				{4.0, 5.0, 6.0},
			},
			output: [][]float64{
				{1.0, 4.0},
				{2.0, 5.0},
				{3.0, 6.0},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// backup input array
			binput := copyTDArray(tc.input)

			output := transpose(tc.input)
			for i := 0; i < len(tc.input); i++ {
				for j := 0; j < len(tc.input[i]); j++ {
					assert.Equal(t, tc.output[j][i], output[j][i], "Unexpected transposed value")
				}
			}

			// validate we did not modify input arrays
			for i := 0; i < len(binput); i++ {
				for j := 0; j < len(binput[i]); j++ {
					assert.Equal(t, tc.input[i][j], binput[i][j], "Unexpected - modified input array")
				}
			}
		})

	}
}

// func subtractvector(v1 []float64, v2 []float64) []float64 {
func TestSubstractVector(t *testing.T) {
	tt := []struct {
		name   string
		input1 []float64
		input2 []float64
		output []float64
	}{
		{
			name:   "Basic substract",
			input1: []float64{1.0, 2.5},
			input2: []float64{1.0, 2.5},
			output: []float64{0.0, 0.0},
		}, {
			name:   "Basic with decimals",
			input1: []float64{1.0, 2.5},
			input2: []float64{1.3, 2.5},
			output: []float64{-0.30001, 0.0},
		}, {
			name:   "input from realdata",
			input1: []float64{-1.233758177597947, -0.8460943734555971},
			input2: []float64{0.20150366623117735, 0.3493197733337192},
			output: []float64{-1.43527, -1.19542},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// backup input arrays
			binput1 := copyODArray(tc.input1)
			binput2 := copyODArray(tc.input2)

			output := subtractvector(tc.input1, tc.input2)
			for i := 0; i < len(tc.output); i++ {
				assert.Equal(t, tc.output[i], math.Floor(output[i]*100000)/100000, "Unexpected substract value of vector")
			}

			// validate we did not modify our input arrays
			for i := 0; i < len(binput1); i++ {
				assert.Equal(t, tc.input1[i], binput1[i], "Unexpected - modified input array 1")
			}

			for i := 0; i < len(binput2); i++ {
				assert.Equal(t, tc.input2[i], binput2[i], "Unexpected - modified input array 2")
			}
		})
	}
}

// func scalevector(val float64, v []float64) []float64 {
func TestScaleVector(t *testing.T) {
	tt := []struct {
		name   string
		scale  float64
		vector []float64
		output []float64
	}{
		{
			name:   "Basic scale",
			scale:  1.0,
			vector: []float64{1.0, 2.5},
			output: []float64{1.0, 2.5},
		}, {
			name:   "Basic scale x0",
			scale:  0.0,
			vector: []float64{1.0, 2.5},
			output: []float64{0.0, 0.0},
		}, {
			name:   "Basic scale x2",
			scale:  2.0,
			vector: []float64{1.0, 2.5},
			output: []float64{2.0, 5.0},
		}, {
			name:   "Basic scale x0.5",
			scale:  0.5,
			vector: []float64{1.0, 5.0},
			output: []float64{0.5, 2.5},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			bvector := copyODArray(tc.vector)

			output := scalevector(tc.scale, tc.vector)
			for i := 0; i < len(tc.output); i++ {
				assert.Equal(t, tc.output[i], output[i], "Unexpected scale value of vector")
			}

			for i := 0; i < len(bvector); i++ {
				assert.Equal(t, tc.vector[i], bvector[i], "Unexpected - modified input scale")
			}
		})

	}
}

// func multiplyVector(v1 []float64, v2 []float64) []float64 {
func TestMultiplyVector(t *testing.T) {
	tt := []struct {
		name   string
		input1 []float64
		input2 []float64
		output []float64
	}{
		{
			name:   "Basic multiply vector",
			input1: []float64{1.0, 2.0},
			input2: []float64{3.0, 4.0},
			output: []float64{3.0, 8.0},
		}, {
			name:   "Basic decimal multiply vector",
			input1: []float64{1.0, 2.5},
			input2: []float64{3.0, 4.0},
			output: []float64{3.0, 10.0},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput1 := copyODArray(tc.input1)
			binput2 := copyODArray(tc.input2)

			output := multiplyVector(tc.input1, tc.input2)
			for i := 0; i < len(tc.output); i++ {
				assert.Equal(t, tc.output[i], output[i], "Unexpected multiplication value of vector")
			}

			for i := 0; i < len(binput1); i++ {
				assert.Equal(t, tc.input1[i], binput1[i], "Unexpected - modified input array 1")
			}

			for i := 0; i < len(binput2); i++ {
				assert.Equal(t, tc.input2[i], binput2[i], "Unexpected - modified input array 2")
			}
		})

	}
}

// func Argmax(args []float64) int {
func TestArgmax(t *testing.T) {
	tt := []struct {
		name   string
		input  []float64
		output int
	}{
		{
			name:   "Basic argmax",
			input:  []float64{0.1, 0.003, 0.8},
			output: 2,
		}, {
			name:   "Basic argmax, shuffle",
			input:  []float64{1.0, 4.0, 6.0, 5.0, 2.0, 0.0},
			output: 2,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput := copyODArray(tc.input)

			output := Argmax(tc.input)
			assert.Equal(t, tc.output, output, "Unexpected argmax value")

			for i := 0; i < len(binput); i++ {
				assert.Equal(t, tc.output, output, "Unexpected - modified input array")
			}
		})

	}
}

// func Sigmoid(z []float64) []float64 {
func TestSigmoid(t *testing.T) {
	tt := []struct {
		name   string
		input  []float64
		output []float64
	}{
		{
			name:   "Basic sigmoid",
			input:  []float64{6.0, -6.0},
			output: []float64{0.9975273768433653, 0.0024726231566347743},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// make copy of our input
			binput := copyODArray(tc.input)

			output := Sigmoid(tc.input)
			for i := 0; i < len(tc.input); i++ {
				assert.Equal(t, tc.output[i], output[i], "Unexpected sigmoid value")
			}

			// check if we modified our input array
			for i := 0; i < len(binput); i++ {
				assert.Equal(t, tc.input[i], binput[i], "Unexpected - modified our input array")
			}
		})

	}
}
