package neuralNetwork

import (
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"time"

	"bitbucket.org/aauitc17/p4gr450nn/readcsv"
)

type Network struct {
	accuracy   string
	num_layers int
	Debug      bool
	sizes      []int
	biases     [][]float64   //list of matrices
	weights    [][][]float64 //list of matrices
}

type Testdata struct {
	Input   [][]float64
	Output  []int
	Present bool
}

func (d *Testdata) Init(data readcsv.Data) {
	d.Present = true
	d.Input = data.Input
	for i := range data.Output {
		for j := range data.Output[i] {
			if data.Output[i][j] == 1 {
				d.Output = append(d.Output, j)
			}
		}
	}
}

//weights and biases are not random, they should be
func (n *Network) Init(sizes []int) {
	n.accuracy = "untrained"
	n.num_layers = len(sizes)
	n.sizes = sizes

	b := sizes[1:]
	w := sizes[:len(sizes)-1]
	vlist := make([][]float64, len(b))
	mlist := make([][][]float64, len(w))

	// if we need to seed, uncomment this line - not secure
	// but for our purpose, OK
	if !n.Debug {
		var randSeed int64
		// randSeed = 1556900598
		randSeed = time.Now().Unix()
		fmt.Printf("Using seed: %d\n", randSeed)
		rand.Seed(randSeed)
	}

	//initialize biases between each layer as a vector
	//only 1 value for each recieving neuron is needed
	for y := range b {
		for j := 0; j < b[y]; j++ {
			vlist[y] = append(vlist[y], rand.NormFloat64())
		}
	}
	n.biases = vlist
	//initialize weights bewteen each layers as matrix
	//1 weight for each connection
	for y := range b {
		for j := 0; j < b[y]; j++ {
			mlist[y] = append(mlist[y], make([]float64, 0))
			for k := 0; k < w[y]; k++ {
				mlist[y][j] = append(mlist[y][j], rand.NormFloat64())
			}
		}
	}
	n.weights = mlist
}

func (n *Network) SGD(training_data readcsv.Data, epochs int, mini_batch_size int, eta float64, test_data Testdata) {
	var d_test int
	if test_data.Present {
		d_test = len(test_data.Input)
	}
	for j := 0; j < epochs; j++ {

		// Shuffle(training_data) //it is not random enough, produces the same results
		training_data = Shuffle(training_data)
		mini_batches := Subdivide(training_data, mini_batch_size)
		// fmt.Println(len(mini_batches[0]))
		for _, mini_batch := range mini_batches {
			n.Update_mini_batch(mini_batch, eta)
		}
		//fmt.Println(n.biases[0])
		if test_data.Present {
			eva := n.Evaluate(test_data)
			n.accuracy = strconv.Itoa(eva) + "OF" + strconv.Itoa(d_test)
			fmt.Println("Epoch", j, ":", eva, "/", d_test)
		} else {
			fmt.Println("Epoch complete", j)
		}
	}
}

//mini_batch is a list as [][]float64 the indermost array is tuples (x ,y)
func (n *Network) Update_mini_batch(mini_batch readcsv.Data, eta float64) {
	//make gradient and set all values to 0
	nabla_b := make([][]float64, len(n.biases))
	for b := range nabla_b {
		for range n.biases[b] {
			nabla_b[b] = append(nabla_b[b], 0.0)
		}
	}
	nabla_w := make([][][]float64, len(n.weights))
	for w := range nabla_w {
		for j := range n.weights[w] {
			nabla_w[w] = append(nabla_w[w], make([]float64, len(n.weights[w][j])))
		}
	}
	for d := range mini_batch.Input {
		var delta_nabla_b, delta_nabla_w = n.Backprop(mini_batch.Input[d], mini_batch.Output[d])
		for b := range nabla_b {
			nabla_b[b] = add(nabla_b[b], delta_nabla_b[b])

		}
		for w := range nabla_w {
			for m := range nabla_w[w] {
				nabla_w[w][m] = add(nabla_w[w][m], delta_nabla_w[w][m])
			}
		}
	}
	for w := range n.weights {
		n.weights[w] = subtractmatrix(n.weights[w], scalematrix((eta/float64(len(mini_batch.Input))), nabla_w[w]))
		//virker måske ikke pga det er matrixer
		//fmt.Print(n.weights[w])
	}
	// fmt.Print(n.biases[0][0])
	// fmt.Print(" - ", scalevector((eta / float64(len(mini_batch.Input))), nabla_b[0])[0])
	// fmt.Println(" = ", subtractvector(n.biases[0], scalevector((eta/float64(len(mini_batch.Input))), nabla_b[0]))[0])
	for b := range n.biases {
		n.biases[b] = subtractvector(n.biases[b], scalevector((eta/float64(len(mini_batch.Input))), nabla_b[b]))
		//virker måske ikke pga det er vektorer
	}
}

func (n *Network) Backprop(x []float64, y []int64) ([][]float64, [][][]float64) {
	//returns a tuple (nabla_b, nabla_w),
	//they are each a layer by layer list of arrays, similar to n.biases and n.weights
	nabla_b := make([][]float64, len(n.biases))
	for b := range nabla_b {
		for range n.biases[b] {
			nabla_b[b] = append(nabla_b[b], 0.0)
		}
	}
	nabla_w := make([][][]float64, len(n.weights))
	for w := range nabla_w {
		for j := range n.weights[w] {
			nabla_w[w] = append(nabla_w[w], make([]float64, len(n.weights[w][j])))
		}
	}
	activation := x               //en activation er en vektor af input
	activations := [][]float64{x} //liste til alle activations, layer by layer
	var zs [][]float64            //liste af all z vectors layer by layer
	var z []float64               //z vector
	for i := range n.biases {
		z = add(dot(n.weights[i], activation), n.biases[i])
		zs = append(zs, z)
		activation = Sigmoid(z)
		activations = append(activations, activation)
	}
	//backward pass
	//activations skal have længde 3
	delta := multiplyVector(n.Costderivative(activations[len(activations)-1], y), Sigmoid_prime(zs[len(zs)-1]))
	nabla_b[len(nabla_b)-1] = delta
	nabla_w[len(nabla_w)-1] = dotTranspose(delta, activations[len(activations)-2])
	for l := 2; l < n.num_layers; l++ {
		z = zs[len(zs)-l]
		sp := Sigmoid_prime(z)
		//matrix 30x10 dot delta vector 1x10
		delta = multiplyVector(dot(transpose(n.weights[len(n.weights)-l+1]), delta), sp)
		nabla_b[len(nabla_b)-l] = delta
		nabla_w[len(nabla_w)-l] = dotTranspose(delta, activations[len(activations)-l-1])
	}
	return nabla_b, nabla_w
}

// test_data.input is an array of len 784 an test_data.output is a number
func (n *Network) Evaluate(test_data Testdata) int {
	test_results := make([][]int, len(test_data.Input))
	for i := range test_data.Input {
		//fmt.Println("arg", Argmax(n.Feedforward(test_data.Input[i])))
		test_results[i] = []int{Argmax(n.Feedforward(test_data.Input[i])), test_data.Output[i]}
	}
	//fmt.Println(test_results)
	var correctguesss int
	for i := range test_results {
		if test_results[i][0] == test_results[i][1] {
			correctguesss++
		}
	}
	return correctguesss
}

func (n *Network) Guess(input []float64) int {
	// guess := Argmax(n.Feedforward(input))
	// guess := n.Feedforward(input)
	// fmt.Println(guess)
	return Argmax(n.Feedforward(input))
	//test_results[i] = []int{, test_data.Output[i]}
	////fmt.Println(test_results)
	//var correctguesss int
	//for i := range test_results {
	//	if test_results[i][0] == test_results[i][1] {
	//		correctguesss++
	//	}
	//}
	//return correctguesss
}

//needs revising input 2 arrays på len 10
func (n *Network) Costderivative(output_activations []float64, y []int64) []float64 {
	//vector of partiel derivatives dC_x/da for the output output_activations
	s := make([]float64, len(output_activations))
	for i := range output_activations {
		s[i] = output_activations[i] - float64(y[i])
	}
	return s
}

func (n *Network) Feedforward(a []float64) []float64 {
	s := append([]float64(nil), a...)
	for i := range n.biases {
		s = Sigmoid(add(dot(n.weights[i], s), n.biases[i]))
	}
	//fmt.Println("after", s) //[0 0 0 0 0 0 0 0 0 0] forventer array af floats
	return s
}

//apply exp elementwise on array, used for activation
func Sigmoid(z []float64) []float64 {
	s := make([]float64, len(z))
	for elm, val := range z {
		s[elm] = 1.0 / (1.0 + math.Exp(-(val)))
	}
	return s
}

//derivative of sigmoid function applied on a vector
func Sigmoid_prime(z []float64) []float64 {
	s := make([]float64, len(z))
	a := Sigmoid(z)
	for i, val := range a {
		s[i] = 1.0 - val
	}
	return multiplyVector(a, s)
}

//used for shuffleing slices - not used for testing
func Shuffle(training_data readcsv.Data) readcsv.Data {
	input := training_data.Input
	output := training_data.Output
	// could use copyTDArray etc. but we really do not care
	// if we modify the data..
	// input := copyTDArray(training_data.Input)
	// output := copyTDArrayInt(training_data.Output)

	rand.Shuffle(len(input), func(i, j int) {
		input[i], input[j] = input[j], input[i]
		output[i], output[j] = output[j], output[i]
	})

	training_data.Input = input
	training_data.Output = output

	return training_data
}

//Del training_data i mini_batches baseret på en mini_batch_size
func Subdivide(training_data readcsv.Data, mini_batch_size int) []readcsv.Data {
	var d []readcsv.Data
	var db readcsv.Data
	for j := 0; j < len(training_data.Input)/mini_batch_size; j++ {
		db.Input = training_data.Input[j*mini_batch_size : (j*mini_batch_size)+mini_batch_size]
		db.Output = training_data.Output[j*mini_batch_size : (j*mini_batch_size)+mini_batch_size]
		d = append(d, db)
	}
	return d
}

func Argmax(args []float64) int {
	max := 0.0
	guess := 0
	for _, arg := range args {
		if arg > max {
			max = arg
		}
	}
	for i, arg := range args {
		if arg == max {
			guess = i
		}
	}
	return guess
}

//dot product of matrix and vector
func dot(w [][]float64, a []float64) []float64 {
	v := make([]float64, len(w))
	for i := 0; i < len(w); i++ {
		for j := 0; j < len(w[i]); j++ {
			v[i] += w[i][j] * a[j]
		}
	}

	return v
}

//multiply 2 vectors together
func multiplyVector(v1 []float64, v2 []float64) []float64 {
	s := make([]float64, len(v1))
	for i := range v1 {
		s[i] = v1[i] * v2[i]
	}
	return s

}

//scale matrix by a value
func scalematrix(val float64, m [][]float64) [][]float64 {
	s := make([][]float64, len(m))
	for i := range m {
		s[i] = make([]float64, len(m[i]))
		for j := range m[i] {
			s[i][j] = val * m[i][j]
		}
	}
	return s
}

func scalevector(val float64, v []float64) []float64 {
	s := make([]float64, len(v))
	for i := range v {
		s[i] = val * v[i]
	}
	return s
}

func subtractmatrix(m1 [][]float64, m2 [][]float64) [][]float64 {
	m := make([][]float64, len(m1))
	for i := range m {
		for j := range m1[i] {
			m[i] = append(m[i], m1[i][j]-m2[i][j])
		}
	}
	return m
}

func subtractvector(v1 []float64, v2 []float64) []float64 {
	v := make([]float64, len(v1))
	for i := range v1 {
		v[i] = v1[i] - v2[i]
	}
	return v
}

//addes 2 vectors together
func add(a []float64, b []float64) []float64 {
	s := make([]float64, len(a))
	for i := range a {
		s[i] = a[i] + b[i]
	}
	return s
}

func transpose(m [][]float64) [][]float64 {
	m2 := make([][]float64, len(m[0]))
	for i := range m2 {
		for j := range m {
			m2[i] = append(m2[i], m[j][i])
		}
	}
	return m2
}

func dotTranspose(v1 []float64, v2 []float64) [][]float64 {
	m := make([][]float64, len(v2))
	for i := range v1 {
		for j := range v2 {
			m[i] = append(m[i], v1[i]*v2[j])
		}
	}
	return m
}
