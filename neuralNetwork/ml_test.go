package neuralNetwork

import (
	"testing"
	"math"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/aauitc17/p4gr450nn/readcsv"
)

func TestSubstractmatrix(t *testing.T) {
	tt := []struct {
		name   string
		input1 [][]float64
		input2 [][]float64
		output [][]float64
	}{
		{
			name: "submatrix",
			input1: [][]float64{
				{0.1, 0.2},
				{0.1, 0.2},
			},
			input2: [][]float64{
				{0.1, 0.2},
				{0.1, 0.2},
			},
			output: [][]float64{
				{0.0, 0.0},
				{0.0, 0.0},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput1 := copyTDArray(tc.input1)
			binput2 := copyTDArray(tc.input2)

			output := subtractmatrix(tc.input1, tc.input2)
			for i := 0; i < len(output); i++ {
				for j := 0; j < len(output[i]); j++ {
					assert.Equal(t, tc.output[i][j], output[i][j], "errur")
				}
			}

			for i := 0; i < len(binput1); i++ {
				for j := 0; j < len(binput1[i]); j++ {
					assert.Equal(t, tc.input1[i][j], binput1[i][j], "Unexpected - modified input array 1")
				}
			}
			for i := 0; i < len(binput2); i++ {
				for j := 0; j < len(binput2[i]); j++ {
					assert.Equal(t, tc.input2[i][j], binput2[i][j], "Unexpected - modified input array 2")
				}
			}
		})

	}
}

func TestDotTranspose(t *testing.T) {
	tt := []struct {
		name   string
		input1 []float64
		input2 []float64
		output [][]float64
	}{
		{
			name:   "dotTranspose",
			input1: []float64{1.0, 2.0, 3.0},
			input2: []float64{1.0, 2.0, 3.0},
			output: [][]float64{
				{1.0, 2.0, 3.0},
				{2.0, 4.0, 6.0},
				{3.0, 6.0, 9.0},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput1 := copyODArray(tc.input1)
			binput2 := copyODArray(tc.input2)

			output := dotTranspose(tc.input1, tc.input2)
			for i := 0; i < len(output); i++ {
				for j := 0; j < len(output[i]); j++ {
					assert.Equal(t, tc.output[i][j], output[i][j], "errur")
				}
			}

			for i := 0; i < len(binput1); i++ {
				assert.Equal(t, tc.input1[i], binput1[i], "Unexpected - modified input array 1")
			}
			for i := 0; i < len(binput2); i++ {
				assert.Equal(t, tc.input2[i], binput2[i], "Unexpected - modified input array 2")
			}
		})

	}
}

func TestScalematrix(t *testing.T) {
	tt := []struct {
		name   string
		input1 float64
		input2 [][]float64
		output [][]float64
	}{
		{
			name:   "scalematrix",
			input1: 2.0,
			input2: [][]float64{
				{1.0, 1.0, 1.0},
				{1.0, 2.0, 1.0},
				{1.0, 1.0, 1.0},
			},

			output: [][]float64{
				{2.0, 2.0, 2.0},
				{2.0, 4.0, 2.0},
				{2.0, 2.0, 2.0},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput2 := copyTDArray(tc.input2)

			output := scalematrix(tc.input1, tc.input2)
			for i := 0; i < len(output); i++ {
				for j := 0; j < len(output[i]); j++ {
					assert.Equal(t, tc.output[i][j], output[i][j], "errur")
				}
			}

			for i := 0; i < len(binput2); i++ {
				for j := 0; j < len(binput2[i]); j++ {
					assert.Equal(t, tc.input2[i][j], binput2[i][j], "Unexpected - Modified input array 2")
				}
			}
		})

	}
}

func TestDot(t *testing.T) {
	tt := []struct {
		name   string
		input1 [][]float64
		input2 []float64
		output []float64
	}{
		{
			name: "scalematrix",
			input1: [][]float64{
				{1.0, 1.0},
				{2.0, 1.0},
				{1.0, 1.0},
			},

			input2: []float64{1.0, 2.0},

			output: []float64{3.0, 4.0, 3.0},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput1 := copyTDArray(tc.input1)

			output := dot(tc.input1, tc.input2)
			for i := 0; i < len(output); i++ {
				assert.Equal(t, tc.output[i], output[i], "errur")
			}

			for i := 0; i < len(binput1); i++ {
				for j := 0; j < len(binput1[i]); j++ {
					assert.Equal(t, tc.input1[i][j], binput1[i][j], "Unexpected - Modified input array 1")
				}
			}
		})

	}
}

// func Subdivide(training_data readcsv.Data, mini_batch_size int) []readcsv.Data {
func TestSubdivide(t *testing.T) {
	tt := []struct {
		name string
        training_data readcsv.Data
        batch_size int
        output []readcsv.Data
	}{
		{
			name:    "Subdivide",
			training_data: readcsv.Data{
				Input: [][]float64{
					{1.1, 1.2, 1.3},{2.1, 2.2, 2.3},{3.1, 3.2, 3.3},
				 	{4.1, 4.2, 4.3},{5.1, 5.2, 5.3},{6.1, 6.2, 6.3},
					{7.1, 7.2, 7.3},{8.1, 8.2, 8.3},{9.1, 9.2, 9.3},
				},
				Output: [][]int64{
					{1, 1, 1},{2, 2, 2},{3, 3, 3},
				 	{4, 4, 4},{5, 5, 5},{6, 6, 6},
				 	{7, 7, 7},{8, 8, 8},{9, 9, 9},
				},
			},
			batch_size: 3,
            output: []readcsv.Data{
				readcsv.Data{
					Input: [][]float64{
						{1.1, 1.2, 1.3},{2.1, 2.2, 2.3},{3.1, 3.2, 3.3},
					},
					Output: [][]int64{
						{1, 1, 1},{2, 2, 2},{3, 3, 3},
					},
				},
				readcsv.Data{
					Input: [][]float64{
					 	{4.1, 4.2, 4.3},{5.1, 5.2, 5.3},{6.1, 6.2, 6.3},
					},
					Output: [][]int64{
					 	{4, 4, 4},{5, 5, 5},{6, 6, 6},
					},
				},
				readcsv.Data{
					Input: [][]float64{
						{7.1, 7.2, 7.3},{8.1, 8.2, 8.3},{9.1, 9.2, 9.3},
					},
					Output: [][]int64{
					 	{7, 7, 7},{8, 8, 8},{9, 9, 9},
					},
				},
			},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
            output := Subdivide(tc.training_data, tc.batch_size)
            for i := 0; i < len(output); i++ {
                assert.Equal(t, tc.output[i], output[i], "errur")
            }
		})

	}
}
//func Sigmoid_prime(z []float64) []float64
func TestSigmoid_prime(t *testing.T) {
	tt := []struct {
		name   	string
		z 		[]float64
		output 	[]float64
	}{
		{
			name: "Sigmoid_prime",
			z:		[]float64{6.0, -6.0, 0.0},
			output: []float64{0.0024665, 0.0024665, 0.25},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			binput1 := copyODArray(tc.z)

			output := Sigmoid_prime(tc.z)
			for i := 0; i < len(output); i++ {
				assert.Equal(t, tc.output[i], math.Floor(output[i]*100000000)/100000000, "errur")
			}

			for i := 0; i < len(binput1); i++ {
				assert.Equal(t, tc.z[i], binput1[i], "Unexpected - Modified input array z")
			}
		})

	}
}
//func (n *Network) Costderivative(output_activations []float64, y []int64) []float64
// func TestCostderivative(t *testing.T) {
// 	tt := []struct {
// 		name string
// 		output_activations []float64
// 		y []float64
// 		output []float64
// 	}{
// 		{
// 			name: "Sigmoid_prime",
// 			output_activations:	[]float64{0.2, 0.1, 0.3},
// 			y: []float64{0, 0, 1},
// 			output: []float64{0.2, 0.1, -0.7},
// 		},
// 	}
//
// 	for _, tc := range tt {
// 		t.Run(tc.name, func(t *testing.T) {
// 			binput1 := copyODArray(tc.output_activations)
//
// 			output := Costderivative(tc.output_activations)
// 			for i := 0; i < len(output); i++ {
// 				assert.Equal(t, tc.output[i], output[i], "errur")
// 			}
//
// 			for i := 0; i < len(binput1); i++ {
// 				assert.Equal(t, tc.output_activations[i], binput1[i], "Unexpected - Modified input array z")
// 			}
// 		})
//
// 	}
// }
