package neuralNetwork

import (
	"bufio"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"strconv"
	"strings"
)

func (n *Network) SaveNetwork(filename string) {
	var biasesCsv []byte
	for i := 0; i < len(n.biases); i++ {
		for j := 0; j < len(n.biases[i]); j++ {
			sFloat := strconv.FormatFloat(n.biases[i][j], 'f', -1, 64)
			biasesCsv = append(biasesCsv, fmt.Sprintf("%s,", sFloat)...)
		}
		biasesCsv = biasesCsv[:len(biasesCsv)-1]
		biasesCsv = append(biasesCsv, "\n"...)
	}

	var weightsCsv []byte
	for i := 0; i < len(n.weights); i++ {
		for j := 0; j < len(n.weights[i]); j++ {
			for k := 0; k < len(n.weights[i][j]); k++ {
				sFloat := strconv.FormatFloat(n.weights[i][j][k], 'f', -1, 64)
				weightsCsv = append(weightsCsv, fmt.Sprintf("%s,", sFloat)...)
			}
			weightsCsv = weightsCsv[:len(weightsCsv)-1]
			weightsCsv = append(weightsCsv, "\n"...)
		}
		weightsCsv = append(weightsCsv, "\n"...)
	}
	weightsCsv = weightsCsv[:len(weightsCsv)-1]

	var finalCsv []byte
	finalCsv = append(finalCsv, biasesCsv...)
	finalCsv = append(finalCsv, "\n"...)
	finalCsv = append(finalCsv, weightsCsv...)

	ioutil.WriteFile(filename, finalCsv, os.ModePerm)
}

func (n *Network) LoadNetwork(filename string) {
	// the first until empty newline, are our biases
	// after that our weights come into play
	file, err := os.Open(filename)
	if err != nil {
		fmt.Printf("Failed to open file, %v\n", err)
		return
	}
	defer file.Close()

	// scanner := bufio.NewScanner(file)
	var weightsStage bool
	var biasesLines [][]float64
	var weightsLines [][][]float64
	var weightsIndex int
	_ = weightsLines
	scanner := bufio.NewReader(file)
	for {
		line, err := scanner.ReadBytes('\n')
		if err == io.EOF {
			break
		}

		line = line[:len(line)-1]
		if len(line) == 0 {
			weightsIndex = 0
			weightsStage = true
			weightsLines = append(weightsLines, [][]float64{})
			continue
		}

		if !weightsStage {
			biasesLines = append(biasesLines, []float64{})
			sLine := strings.Split(string(line), ",")
			for _, val := range sLine {
				fval, _ := strconv.ParseFloat(val, 64)
				biasesLines[len(biasesLines)-1] = append(biasesLines[len(biasesLines)-1], fval)

			}
		} else {
			weightsLines[len(weightsLines)-1] = append(weightsLines[len(weightsLines)-1], []float64{})
			sLine := strings.Split(string(line), ",")
			for _, val := range sLine {
				fval, _ := strconv.ParseFloat(val, 64)
				weightsLines[len(weightsLines)-1][weightsIndex] = append(weightsLines[len(weightsLines)-1][weightsIndex], fval)

			}
			weightsIndex++
		}
	}

	// // USED TO VALIDATE
	// fmt.Println(n.biases[0][0])
	// fmt.Println(biasesLines[0][0])
	// for i := 0; i < len(n.biases); i++ {
	// 	for j := 0; j < len(n.biases[i]); j++ {
	// 		if n.biases[i][j] != biasesLines[i][j] {
	// 			fmt.Println("Error!")
	// 		}
	// 	}
	// }

	// for i := 0; i < len(n.weights); i++ {
	// 	for j := 0; j < len(n.weights[i]); j++ {
	// 		for k := 0; k < len(n.weights[i][j]); k++ {
	// 			if n.weights[i][j][k] != weightsLines[i][j][k] {
	// 				fmt.Println("Error!")
	// 			}
	// 		}
	// 	}
	// }

	n.weights = weightsLines
	n.biases = biasesLines
}
