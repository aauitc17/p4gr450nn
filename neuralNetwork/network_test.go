package neuralNetwork

import (
	"fmt"
	"math/rand"
	"testing"

	"bitbucket.org/aauitc17/p4gr450nn/readcsv"
	"github.com/stretchr/testify/assert"
)

// 1011
// 0011
// 1011
// 1011
// 0010
// 0010
// 0010

// type readcsv.Data struct {
//     Input   [][]float64
//     Output  [][]int64
// }
func generateData(entries int, size int) ([][]float64, [][]int64) {
	input := make([][]float64, entries)
	output := make([][]int64, entries)

	// always seed the same!
	rand.Seed(1)

	for i := 0; i < len(input); i++ {
		input[i] = make([]float64, size)
		output[i] = make([]int64, 2)
		for j := 0; j < size; j++ {
			if rand.Intn(10) > 4 {
				input[i][j] = 1.0
			} else {
				input[i][j] = 0.0
			}
		}

		if int64(input[i][0]) == 1 {
			output[i][0] = 1
		} else {
			output[i][1] = 1
		}
	}

	// fmt.Printf("Input: [][]foat64{\n")
	// for _, i := range input {
	// 	fmt.Printf("\t{")
	// 	for _, v := range i {
	// 		fmt.Printf("%f, ", v)
	// 	}
	// 	fmt.Printf("}")
	// 	fmt.Printf("\n")
	// }
	// fmt.Printf("}\n")

	// fmt.Printf("Output: []int64{\n")
	// for _, i := range output {
	// 	fmt.Printf("\t{")
	// 	for _, v := range i {
	// 		fmt.Printf("%d, ", v)
	// 	}
	// 	fmt.Printf("}\n")
	// }
	// fmt.Printf("}\n")
	// using output below nromalley
	// Input: [][]foat64{
	// {0.000000, 1.000000, 1.000000, 1.000000, 0.000000, 1.000000, 1.000000, 0.000000, }
	// {1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 1.000000, 0.000000, }
	// {0.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, 1.000000, }
	// {1.000000, 1.000000, 1.000000, 1.000000, 0.000000, 1.000000, 0.000000, 1.000000, }
	// {1.000000, 0.000000, 1.000000, 1.000000, 1.000000, 0.000000, 1.000000, 1.000000, }
	// {0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000, 1.000000, 0.000000, }
	// {1.000000, 0.000000, 1.000000, 0.000000, 1.000000, 1.000000, 0.000000, 1.000000, }
	// {1.000000, 1.000000, 0.000000, 1.000000, 0.000000, 0.000000, 1.000000, 1.000000, }
	// }
	// Output: []int64{
	// {0, 1}
	// {1, 0}
	// {0, 1}
	// {1, 0}
	// {1, 0}
	// {0, 1}
	// {1, 0}
	// {1, 0}
	// }

	return input, output
}

func TestInit(t *testing.T) {
	tt := []struct {
		name       string
		sizes      []int
		outSize    int
		outLayers  int
		outBiases  []int // length of our biases
		outWeights []int // length of our weights
	}{
		{
			name:       "Basic init",
			sizes:      []int{8, 4, 2},
			outSize:    3,
			outBiases:  []int{2, 4, 2},
			outWeights: []int{2, 4, 2},
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// init our network
			var net Network
			net.Debug = true
			net.Init(tc.sizes)

			// just check for number of layers
			assert.Equal(t, tc.outSize, net.num_layers, "Unexpected number of layers")

			// check that it initialised everything correct regarding biases
			assert.Equal(t, tc.outBiases[0], len(net.biases), "Unexpected number of biases")
			for i := 0; i < len(net.biases); i++ {
				assert.Equal(t, tc.outBiases[i+1], len(net.biases[i]), "Unexpected length of our biases")
			}

			// check iniitialised of weights
			assert.Equal(t, tc.outWeights[0], len(net.weights), "Unexpected number of Weights")
			for i := 0; i < len(net.weights); i++ {
				assert.Equal(t, tc.outWeights[i+1], len(net.weights[i]), "Unexpected length of our Weights")
			}

		})

	}
}

// type readcsv.Data struct {
//     Input   [][]float64
//     Output  [][]int64
// }

func TestDataInit(t *testing.T) {
	// get our testInput
	testInput, testOutput := generateData(10, 8)

	// init a testdata
	var data Testdata

	// some readcsv data
	csvData := readcsv.Data{
		Input:  testInput,
		Output: testOutput,
	}

	// init our data
	data.Init(csvData)

	// test it
	for i := 0; i < len(testInput); i++ {
		for j := 0; j < len(testInput[i]); j++ {
			assert.Equal(t, testInput[i][j], data.Input[i][j], "Unexpected - unexpected input")
		}
	}

	for i := 0; i < len(testOutput); i++ {
		for j := 0; j < len(testOutput[i]); j++ {
			if testOutput[i][j] == 1 {
				assert.Equal(t, data.Output[i], j, "Unexpected - unexpected input")
			}
		}
	}
}

func TestFeedforward(t *testing.T) {
	// get our testInput
	testInput, testOutput := generateData(10, 8)

	// init our testdata
	var data Testdata
	csvData := readcsv.Data{
		Input:  testInput,
		Output: testOutput,
	}
	data.Init(csvData)

	var net Network
	net.Init([]int{8, 4, 2})

	input := []float64{0, 1, 2, 3, 4, 5, 6, 7}
	fmt.Println(input)
	a := net.Feedforward(input)
	_ = a
	fmt.Println(input)
	// fmt.Println(a)
}
