package main

import (
	"bitbucket.org/aauitc17/p4gr450nn/neuralNetwork"
	"bitbucket.org/aauitc17/p4gr450nn/readcsv"
)

func main() {
	// trainds := "readcsv/data.csv"
	// testds := "readcsv/testdata.csv"
	// trainds := "readcsv/fin.csv"
	// testds := "readcsv/val.csv"
	// trainds := "readcsv/merged-shuffle-2-fin.csv"
	// testds := "readcsv/merged-shuffle-2-val.csv"
	// trainds := "readcsv/new-merged-shuffle-fin.csv"
	// testds := "readcsv/new-merged-shuffle-val.csv"
	trainds := "readcsv/train.csv"
	testds := "readcsv/val.csv"
	size := []int{64 * 64, 40, 4}
	// size := []int{768, 30, 10}

	var net neuralNetwork.Network        //make object for network
	var training_data readcsv.Data       //make object for training_data
	var testdata readcsv.Data            //make object for testdata read from csv
	var test_data neuralNetwork.Testdata //make object for test_data to parse to network
	training_data.Readdata(trainds)
	testdata.Readdata(testds) //get training_data

	//make testdata should not be made from training_data, but can be
	test_data.Init(testdata)
	_ = test_data
	// net.Debug = true
	net.Init(size)

	// read test image
	// data, _ := ioutil.ReadFile("testimage.csv")
	// data, _ := ioutil.ReadFile("testimages60-rpi.csv")
	// data, _ := ioutil.ReadFile("testimage80.csv")
	// data, _ := ioutil.ReadFile("testimages80-rpi.csv")
	// data, _ := ioutil.ReadFile("testimage110.csv")
	// data, _ := ioutil.ReadFile("testimages110-rpi.csv")
	// data, _ := ioutil.ReadFile("testimage130.csv")
	// data, _ := ioutil.ReadFile("testimages130-rpi.csv")
	// dSplit := strings.Split(string(data), ",")
	// var fImage []float64
	// for _, val := range dSplit {
	// 	fval, _ := strconv.ParseFloat(val, 64)
	// 	fImage = append(fImage, fval)
	// }
	// fmt.Printf("Length of image: %d\n", len(fImage))

	//SGD(training_data readcsv.Data, epochs int, mini_batch_size int, eta float64, test_data Testdata)
	// net.SGD(training_data, 1, 10, 1.7, test_data)
	// folder := net.SaveNetTo()
	// fmt.Println("saved to ", folder)
	// net.LoadNetFrom(folder)
	// net.LoadNetwork("network-increased-300.csv")
	// net.LoadNetwork("test-NEW2.csv")
	// net.LoadNetwork("network-based-on-graphs2.csv")
	// start := time.Now()
	// fmt.Println(net.Evaluate(test_data))
	// fmt.Println(time.Since(start))

	// 	fmt.Println("-----")
	// 	net.Guess(fImage)

	// net.SGD(training_data, 15, 4, 3.0, test_data)
	net.SGD(training_data, 15, 10, 1.3, test_data)
	net.SaveNetwork("network-based-on-graphs3.csv")
	//training_data.Printdata()
	// z := []float64{0.0, -0.654657, 0.456457}
	// fmt.Println(neuralNetwork.Sigmoid(z))
}

// Using seed: 1557326354
// Epoch 0 : 957 / 1028
// Epoch 1 : 948 / 1028
// Epoch 2 : 985 / 1028
// Epoch 3 : 991 / 1028
// Epoch 4 : 994 / 1028
// Epoch 5 : 985 / 1028
// Epoch 6 : 995 / 1028
// Epoch 7 : 993 / 1028
// Epoch 8 : 991 / 1028
// Epoch 9 : 995 / 1028

// size := []int{64 * 64, 150, 4}
// net.SGD(training_data, 30, 10, 1.3, test_data)
// Using seed: 1557311161
// Epoch 0 : 90 / 246
// Epoch 1 : 196 / 246
// Epoch 2 : 206 / 246
// Epoch 3 : 192 / 246
// Epoch 4 : 220 / 246
// Epoch 5 : 232 / 246
// Epoch 6 : 225 / 246
// Epoch 7 : 220 / 246
// Epoch 8 : 237 / 246
// Epoch 9 : 234 / 246
// Epoch 10 : 240 / 246
// Epoch 11 : 241 / 246
// Epoch 12 : 242 / 246
// Epoch 13 : 228 / 246
// Epoch 14 : 233 / 246
// Epoch 15 : 243 / 246
// Epoch 16 : 235 / 246
// Epoch 17 : 243 / 246
// Epoch 18 : 246 / 246
// Epoch 19 : 245 / 246
// Epoch 20 : 245 / 246
// Epoch 21 : 246 / 246
// Epoch 22 : 246 / 246
// Epoch 23 : 245 / 246
// Epoch 24 : 246 / 246
// Epoch 25 : 246 / 246
// Epoch 26 : 246 / 246
// Epoch 27 : 245 / 246
// Epoch 28 : 246 / 246
// Epoch 29 : 246 / 246

// seems good Using seed: 1556904187
// ETA at 1.3

// size := []int{64 * 64, 150, 4}
// net.SGD(training_data, 50, 10, 1.5, test_data)
// Using seed: 1556900598
// Epoch 0 : 158 / 246
// Epoch 1 : 192 / 246
// Epoch 2 : 189 / 246
// Epoch 3 : 194 / 246
// Epoch 4 : 195 / 246
// Epoch 5 : 197 / 246
// Epoch 6 : 205 / 246
// Epoch 7 : 201 / 246
// Epoch 8 : 198 / 246
// Epoch 9 : 208 / 246
// Epoch 10 : 202 / 246
// Epoch 11 : 200 / 246
// Epoch 12 : 209 / 246
// Epoch 13 : 199 / 246
// Epoch 14 : 204 / 246
// Epoch 15 : 206 / 246
// Epoch 16 : 204 / 246
// Epoch 17 : 207 / 246
// Epoch 18 : 210 / 246
// Epoch 19 : 239 / 246
// Epoch 20 : 237 / 246
// Epoch 21 : 240 / 246
// Epoch 22 : 239 / 246
// Epoch 23 : 244 / 246
// Epoch 24 : 244 / 246
// Epoch 25 : 244 / 246
// Epoch 26 : 239 / 246
// Epoch 27 : 242 / 246
// Epoch 28 : 242 / 246
// Epoch 29 : 245 / 246
// Epoch 30 : 240 / 246
// Epoch 31 : 241 / 246
// Epoch 32 : 227 / 246
// Epoch 33 : 167 / 246
// Epoch 34 : 231 / 246
// Epoch 35 : 233 / 246
// Epoch 36 : 242 / 246
// Epoch 37 : 235 / 246
// Epoch 38 : 241 / 246
// Epoch 39 : 239 / 246
// Epoch 40 : 244 / 246
// Epoch 41 : 235 / 246
// Epoch 42 : 236 / 246
// Epoch 43 : 245 / 246
// Epoch 44 : 236 / 246
// Epoch 45 : 240 / 246
// Epoch 46 : 237 / 246
// Epoch 47 : 240 / 246
// Epoch 48 : 238 / 246
// Epoch 49 : 242 / 246

// size := []int{64 * 64, 150, 4}
// net.SGD(training_data, 30, 10, 2.0, test_data)
// Using seed: 1556894634
// Epoch 0 : 99 / 246
// Epoch 1 : 160 / 246
// Epoch 2 : 130 / 246
// Epoch 3 : 184 / 246
// Epoch 4 : 179 / 246
// Epoch 5 : 153 / 246
// Epoch 6 : 172 / 246
// Epoch 7 : 174 / 246
// Epoch 8 : 191 / 246
// Epoch 9 : 167 / 246
// Epoch 10 : 144 / 246
// Epoch 11 : 158 / 246
// Epoch 12 : 142 / 246
// Epoch 13 : 157 / 246
// Epoch 14 : 143 / 246
// Epoch 15 : 146 / 246
// Epoch 16 : 147 / 246
// Epoch 17 : 169 / 246
// Epoch 18 : 123 / 246
// Epoch 19 : 141 / 246
// Epoch 20 : 134 / 246
// Epoch 21 : 144 / 246
// Epoch 22 : 160 / 246
// Epoch 23 : 159 / 246
// Epoch 24 : 184 / 246
// Epoch 25 : 160 / 246
// Epoch 26 : 151 / 246
// Epoch 27 : 165 / 246
// Epoch 28 : 179 / 246
// Epoch 29 : 139 / 246

// 150 nodes, 3.0 ETA
// Using seed: 1556893452
// Epoch 0 : 96 / 246
// Epoch 1 : 37 / 246
// Epoch 2 : 60 / 246
// Epoch 3 : 35 / 246
// Epoch 4 : 36 / 246
// Epoch 5 : 36 / 246
// Epoch 6 : 69 / 246
// Epoch 7 : 59 / 246
// Epoch 8 : 39 / 246
// Epoch 9 : 97 / 246
// Epoch 10 : 105 / 246
// Epoch 11 : 107 / 246
// Epoch 12 : 96 / 246
// Epoch 13 : 80 / 246
// Epoch 14 : 35 / 246
// Epoch 15 : 125 / 246
// Epoch 16 : 79 / 246
// Epoch 17 : 134 / 246
// Epoch 18 : 123 / 246
// Epoch 19 : 90 / 246
// Epoch 20 : 90 / 246
// Epoch 21 : 78 / 246
// Epoch 22 : 89 / 246
// Epoch 23 : 116 / 246
// Epoch 24 : 39 / 246
// Epoch 25 : 94 / 246
// Epoch 26 : 81 / 246
// Epoch 27 : 151 / 246
// Epoch 28 : 70 / 246
