package readcsv

import (
	"bufio"
	"bytes"

	//"encoding/json"
	"fmt"
	"io"
	"os"
	"strconv"
)

type Data struct {
	Input  [][]float64
	Output [][]int64
}

func (n *Data) Readdata(path string) {
	csvFile, _ := os.Open(path)

	scanner := bufio.NewReader(csvFile)
	for {
		// read until we get a newline
		line, err := scanner.ReadBytes('\n')
		if err == io.EOF {
			break
		}
		// remove our newline (as it includes it!)
		line = line[:len(line)-1]

		// each line is a new input and output, so add this!
		n.Input = append(n.Input, []float64{})
		// output is a slice that is 10 big (could be dynamic)
		n.Output = append(n.Output, make([]int64, 10))

		// this basically just keeps track of which index we need to use
		inLen := len(n.Input) - 1
		outLen := len(n.Output) - 1

		// split the line up for each comma `,`
		sLine := bytes.Split(line, []byte(","))
		for _, val := range sLine {
			// parse the float!
			fval, _ := strconv.ParseFloat(string(val), 64)
			// append to our current input
			n.Input[inLen] = append(n.Input[inLen], fval)
		}
		// index of our output array we need to sit (was a really ugly one line before)
		// this is basically just the last element in our input array (which indicates what it is)
		index := int(n.Input[inLen][len(n.Input[inLen])-1])
		// set that index to 1 (indicating this is of that type e.g. []int{0,0,1,0,0,0,0,0,0,0}
		n.Output[outLen][index] = 1
		// remove the last element from our input slice, as it is not part of
		// the actual image
		n.Input[inLen] = n.Input[inLen][:len(n.Input[inLen])-1]

	}
}

func (n *Data) Printdata() {
	fmt.Println(len(n.Input))
	fmt.Println(n.Input[0], n.Input[1])
	fmt.Println(len(n.Output))
	fmt.Println(n.Output[0], n.Output[1])
}
